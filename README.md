# wxpay_plus (微信支付plus)

本组件是对微信支付SDK的二次封装，代码中包含了微信支付SDK部分。使用本组件，可以极大减少微信支付对接的代码量。

本支付封装组件支持小程序支付和本地支付（扫码支付）两种方式。

## 组件集成

1.下载本代码，使用maven的install命令安装到本地仓库。

2.在你工程的pom文件中添加依赖。

```xml
<dependency>
    <groupId>com.github</groupId>
    <artifactId>wxpay_plus</artifactId>
    <version>1.0.0</version>
</dependency>
```

3.在配置文件中，添加支付相关的配置，如以下格式：

```yaml
wxpay:
  appId: 
  appSecret: app密钥
  mchId: 商户号
  partnerKey: 商户密钥
  notifyUrl: 回调地址
```

4.修改springboot启动类@SpringBootApplication注解的scanBasePackages属性，添加com.github.wxpay包。

```java
@SpringBootApplication(scanBasePackages = {"com.lkd","com.github.wxpay"})
public class OrderServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderServiceApplication.class, args);
    }
}
```



## 方法调用

### 下单支付

在下单的类中注入 WxPayTemplate

```java
@Autowired
private WxPayTemplate wxPayTemplate;
```

在下单的方法中，只需一下几句即可实现微信支付的下单

```java
//封装支付请求参数
WxPayParam wxPayParam=new WxPayParam();
wxPayParam.setBody(orderEntity.getSkuName());//商品名称
wxPayParam.setOutTradeNo(orderEntity.getOrderNo());//订单号
wxPayParam.setTotalFee(orderEntity.getAmount().intValue());//金额
wxPayParam.setOpenid(orderEntity.getOpenId());//用户id
//调用微信支付
return wxPayTemplate.requestPay( wxPayParam);  
```

如上代码，将requestPay方法返回的内容返回给前端即可。

如果是本地支付（扫码支付），openid属性传空字符串即可。

### 支付回调

wxPayTemplate的validPay方法用于判断支付回调，可参考以下代码

```java
/**
 * 微信支付回调接口
 * @param request
 * @return
 */
@RequestMapping("/payNotify")
@ResponseBody
public void payNotify(HttpServletRequest request, HttpServletResponse response){
  
    try {
        Map<String, String> result = wxPayTemplate.validPay(request.getInputStream());
        if("SUCCESS".equals( result.get("code") )){  //返回码成功
            log.info("修改订单状态和支付状态");
            String orderSn= result.get("order_sn");//获取订单号
            //todo:成功的逻辑处理
        }
    }catch (Exception e){
        log.error("支付回调处理失败",e);
    }
}
```

