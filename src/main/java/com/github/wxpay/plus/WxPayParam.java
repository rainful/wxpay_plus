package com.github.wxpay.plus;

import lombok.Data;

/**
 * 微信支付参数
 */
@Data
public class WxPayParam {

    private String body;//商品描述
    private String outTradeNo; //订单号
    private int totalFee; //订单金额
    private String openid;

}
